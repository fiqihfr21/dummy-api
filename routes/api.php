<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EventController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//ISSUER
Route::get('all-event',[EventController::class, 'getAllEvent']);
Route::post('event-by-address',[EventController::class, 'getEventByAddress']);
Route::post('store-event',[EventController::class, 'storeEvent']);
Route::post('store-order',[EventController::class, 'storeOrder']);
Route::post('order',[EventController::class, 'getOrder']);
Route::post('customer',[EventController::class, 'getCustomer']);

//END USER
Route::post('ticket-customer',[EventController::class, 'getTicketCustomer']);
