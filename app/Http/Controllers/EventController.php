<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;

class EventController extends Controller
{
    //ISSUER

    public function getAllEvent()
    {
        $data = Event::orderBy('id', 'desc')->get();
        
        return response()->json($data, 200);
        // return $this->SendResponse($data, 200);
    }

    public function getEventByAddress(Request $request)
    {
        $data = Event::where('creator_address', $request->creator_address)->orderBy('id', 'desc')->get();
        return response()->json($data, 200);
        // return $this->SendResponse($data, 200);
    }

    public function storeEvent(Request $request)
    {
        $data = new Event;
        $data->contract_address = $request->contract_address;
        $data->creator_address = $request->creator_address;
        $data->name = $request->name;
        $data->description = $request->description;
        $data->image = $request->image;
        $data->place = $request->place;
        $data->date = $request->date;
        $data->save();

        return response()->json($data, 200);
        // return $this->SendResponse($data, 200);
    }

    public function storeOrder(Request $request)
    {
        $data = new Order;
        $data->ticket_id = $request->ticket_id;
        $data->contract_address = $request->contract_address;
        $data->tx_hash = $request->tx_hash;
        $data->address_user = $request->address_user;
        $data->price = $request->price;
        $data->status = $request->status;
        $data->save();

        if(!User::where('address', $request->address_user)->first()){
            $user = new User;
            $user->address = $request->address_user;
            $user->email = $request->email_user;
            $user->save();
        }

        return response()->json($data, 200);
        // return $this->SendResponse($data, 200);
    }

    public function getOrder(Request $request)
    {
        $data = Order::join('events', 'events.contract_address','orders.contract_address')->where('events.creator_address', $request->creator_address)->orderBy('orders.id', 'desc')->get();
        return response()->json($data, 200);
        // return $this->SendResponse($data, 200);
    }

    public function getCustomer(Request $request)
    {
        $data = User::join('orders', 'users.address','orders.address_user')->join('events', 'events.contract_address','orders.contract_address')->where('events.creator_address', $request->creator_address)->orderBy('users.id', 'desc')->get();
        return response()->json($data, 200);
        // return $this->SendResponse($data, 200);
    }


    //END USER

    public function getTicketCustomer(Request $request)
    {
        $data = Order::join('events', 'events.contract_address','orders.contract_address')->where('address_user', $request->user_address)->orderBy('orders.id', 'desc')->get();
        return response()->json($data, 200);
        // return $this->SendResponse($data, 200);
    }

    private function SendResponse($data, $statusCode)
    {
        $response['Error'] = false;
        $response['Message'] = 'success';
        $response['Data'] = $data;
        
        return response()->json($response, $statusCode);
    }
}
